package com.atlassian.webhooks.internal.rest.history;

import com.atlassian.webhooks.history.InvocationCounts;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;

@JsonSerialize
public class RestInvocationCounts extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") //Required by Jersey
    public RestInvocationCounts() {
    }

    public RestInvocationCounts(@Nonnull InvocationCounts counts) {
        put("errors", counts.getErrors());
        put("failures", counts.getFailures());
        put("successes", counts.getSuccesses());
        put("window", ImmutableMap.of(
                "start", counts.getWindowStart().toEpochMilli(),
                "duration", counts.getWindowDuration().toMillis()));
    }
}
