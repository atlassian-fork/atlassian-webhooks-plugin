package com.atlassian.webhooks.internal.rest.history;

import com.atlassian.webhooks.history.InvocationRequest;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;

/**
 * A record of a previously completed {@link com.atlassian.webhooks.WebhookInvocation}
 */
public class RestInvocationRequest extends LinkedHashMap<String, Object> {

    public static RestInvocationRequest EXAMPLE = new RestInvocationRequest(
            "POST", "http://example.com/callback"
    );
    private final String METHOD = "method";
    private final String URL = "url";

    @SuppressWarnings("unused") //Required by Jersey
    public RestInvocationRequest() {
    }

    public RestInvocationRequest(@Nonnull InvocationRequest request) {
        put(METHOD, request.getMethod());
        put(URL, request.getUrl());
    }

    public RestInvocationRequest(String method, String url) {
        put(METHOD, method);
        put(URL, url);
    }
}
