package com.atlassian.webhooks.internal.rest;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.internal.rest.history.RestInvocationHistory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@JsonSerialize
public class RestWebhook extends LinkedHashMap<String, Object> {

    private static final String ACTIVE = "active";
    private static final String CONFIGURATION = "configuration";
    private static final String CREATED_DATE = "createdDate";
    private static final String EVENTS = "events";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String STATISTICS = "statistics";
    private static final String UPDATED_DATE = "updatedDate";
    private static final String URL = "url";

    private static final Date exampleDate = Date.from(Instant.ofEpochSecond(1513106011L));

    public static RestWebhook EXAMPLE = new RestWebhook(
            10, "Webhook Name", exampleDate, exampleDate, ImmutableSet.of("repo:refs_changed", "repo:modified"),
            ImmutableMap.of("secret", "password"), "http://example.com", true);

    @SuppressWarnings("unused") //Required by Jersey
    public RestWebhook() {
    }

    public RestWebhook(Webhook webhook) {
        if (webhook.getId() > 0) {
            put(ID, webhook.getId());
        }
        put(NAME, webhook.getName());
        put(CREATED_DATE, webhook.getCreatedDate());
        put(UPDATED_DATE, webhook.getUpdatedDate());
        put(EVENTS, webhook.getEvents().stream()
                .map(WebhookEvent::getId)
                .collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet)));
        put(CONFIGURATION, webhook.getConfiguration());
        put(URL, webhook.getUrl());
        put(ACTIVE, webhook.isActive());
    }

    private RestWebhook(int id, String name, Date createdDate, Date updatedDate, Set<String> events,
                        Map<String, String> configuration, String url, boolean isActive) {
        put(ID, id);
        put(NAME, name);
        put(CREATED_DATE, createdDate);
        put(UPDATED_DATE, updatedDate);
        put(EVENTS, events);
        put(CONFIGURATION, configuration);
        put(URL, url);
        put(ACTIVE, isActive);
    }

    @Nullable
    public Boolean getActive() {
        Object value = get(ACTIVE);
        return value instanceof Boolean ? (Boolean) value : null;
    }

    @Nonnull
    public Map<String, String> getConfiguration() {
        Object config = get(CONFIGURATION);
        if (config instanceof Map) {
            //noinspection unchecked
            return (Map<String, String>) config;
        }
        return Collections.emptyMap();
    }

    @Nonnull
    public Set<String> getEvents() {
        Object events = get(EVENTS);
        if (events instanceof Collection) {
            //noinspection unchecked
            return new HashSet<>((Collection) events);
        }
        return Collections.emptySet();
    }

    public String getName() {
        return StringUtils.trimToNull(getStringProperty(NAME));
    }

    public String getUrl() {
        return StringUtils.trimToNull(getStringProperty(URL));
    }

    public void setStatistics(RestInvocationHistory statistics) {
        if (statistics != null) {
            put(STATISTICS, statistics);
        }
    }

    private String getStringProperty(String key) {
        return Objects.toString(get(key), null);
    }
}
