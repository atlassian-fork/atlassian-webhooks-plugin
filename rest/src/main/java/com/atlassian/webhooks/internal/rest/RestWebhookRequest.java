package com.atlassian.webhooks.internal.rest;

import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.google.common.collect.ImmutableMap;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

@JsonSerialize
public class RestWebhookRequest extends LinkedHashMap<String, Object> {

    public static RestWebhookRequest EXAMPLE = new RestWebhookRequest(
            "http://example.com/callback", null, "POST", ImmutableMap.of("Accept", "*/*")
    );

    private final String BODY = "body";
    private final String HEADERS = "headers";
    private final String METHOD = "method";
    private final String URL = "url";

    @SuppressWarnings("unused") //Required by Jersey
    public RestWebhookRequest() {
    }

    public RestWebhookRequest(WebhookHttpRequest request) {
        put(URL, request.getUrl());
        put(BODY, request.getContent() != null ? new String(request.getContent(), StandardCharsets.UTF_8) : null);
        put(METHOD, request.getMethod());
        put(HEADERS, request.getHeaders());
    }

    private RestWebhookRequest(String url, String body, String method, Map<String, String> headers) {
        put(URL, url);
        put(BODY, body);
        put(METHOD, method);
        put(HEADERS, headers);
    }
}
