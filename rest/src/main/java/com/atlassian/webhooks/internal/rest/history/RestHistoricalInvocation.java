package com.atlassian.webhooks.internal.rest.history;

import com.atlassian.webhooks.history.HistoricalInvocation;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;

/**
 * A record of a previously completed {@link com.atlassian.webhooks.WebhookInvocation}
 */
@JsonSerialize
public class RestHistoricalInvocation extends LinkedHashMap<String, Object> {

    public static final RestHistoricalInvocation EXAMPLE_ERROR = new RestHistoricalInvocation(
            12, "repo:modified", 100, 1513106011, 1513106111,
            RestInvocationRequest.EXAMPLE, RestInvocationResult.EXAMPLE_ERROR
    );
    public static final RestHistoricalInvocation EXAMPLE_FAILURE = new RestHistoricalInvocation(
            11, "repo:refs_changed", 100, 1513106011, 1513106111,
            RestInvocationRequest.EXAMPLE, RestInvocationResult.EXAMPLE_FAILURE
    );
    public static final RestHistoricalInvocation EXAMPLE_SUCCESS = new RestHistoricalInvocation(
            10, "repo:created", 155, 1513106011, 1513106166,
            RestInvocationRequest.EXAMPLE, RestInvocationResult.EXAMPLE
    );
    private final String DURATION = "duration";
    private final String EVENT = "event";
    private final String FINISH = "finish";
    private final String ID = "id";
    private final String REQUEST = "request";
    private final String RESULT = "result";
    private final String START = "start";

    @SuppressWarnings("unused") //Required by Jersey
    public RestHistoricalInvocation() {
    }

    public RestHistoricalInvocation(@Nonnull HistoricalInvocation invocation) {
        put(ID, invocation.getId());
        put(EVENT, invocation.getEvent().getId());
        put(DURATION, invocation.getDuration().toMillis());
        put(START, invocation.getStart().toEpochMilli());
        put(FINISH, invocation.getFinish().toEpochMilli());
        putRequest(invocation);
        putResponse(invocation);
    }

    private RestHistoricalInvocation(int id, String event, int duration, int start, int finish,
                                    RestInvocationRequest request, RestInvocationResult result) {
        put(ID, id);
        put(EVENT, event);
        put(DURATION, duration);
        put(START, start);
        put(FINISH, finish);
        put(REQUEST, request);
        put(RESULT, result);
    }

    protected void putRequest(@Nonnull HistoricalInvocation invocation) {
        put(REQUEST, new RestInvocationRequest(invocation.getRequest()));
    }

    protected void putResponse(@Nonnull HistoricalInvocation invocation) {
        put(RESULT, new RestInvocationResult(invocation.getResult()));
    }
}
