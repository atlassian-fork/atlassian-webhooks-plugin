package com.atlassian.webhooks.plugin.test.servlet;

public class LegacyEventServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "LegacyEventsQueue";

    protected LegacyEventServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }
}
