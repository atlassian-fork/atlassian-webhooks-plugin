package com.atlassian.webhooks.plugin.junit;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import com.atlassian.webhooks.api.register.listener.WebHookListenerServiceResponse;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import org.junit.Test;

import static com.atlassian.webhooks.plugin.management.WebHookListenerActionValidatorImpl.INVALID_WEBHOOK_NAME;
import static org.junit.Assert.assertFalse;

public class TestWebHookActionValidation extends WebHookUnitTest
{
    @Test
    public void testRegisteringWebHookWithInvalidParameters()
    {
        final WebHookListenerServiceResponse webHookListenerServiceResponse = ServiceAccessor.getWebHookListenerService().registerWebHookListener(
                PersistentWebHookListener.newlyCreated().setUrl("/").addWebHookId("test_event").setListenerName(INVALID_WEBHOOK_NAME).build(), RegistrationMethod.SERVICE);

        assertFalse(webHookListenerServiceResponse.getMessageCollection().isEmpty());
    }
}
