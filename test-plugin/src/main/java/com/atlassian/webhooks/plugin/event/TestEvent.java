package com.atlassian.webhooks.plugin.event;

import static com.google.common.base.Preconditions.*;

public class TestEvent
{
    public final String value;

    public TestEvent(String value)
    {
        this.value = checkNotNull(value);
    }
}
