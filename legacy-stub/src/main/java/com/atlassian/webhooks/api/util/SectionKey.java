package com.atlassian.webhooks.api.util;

import javax.annotation.concurrent.Immutable;

@Deprecated
@Immutable
public final class SectionKey extends TypeRichString {
    public SectionKey(String id) {
        super(id);
    }
}
