package com.atlassian.webhooks.api.util;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.spi.WebHookListenerAccessVoter;

import javax.annotation.Nonnull;

/**
 * Convenience Base implementation of {@link com.atlassian.webhooks.spi.WebHookListenerAccessVoter} that provides default implementations (return {@link Vote#ABSTAIN}) for all voter
 * methods.
 */
@Deprecated
public abstract class AbstractWebHookListenerAccessVoter implements WebHookListenerAccessVoter {
    @Nonnull
    @Override
    public Vote canCreate(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel) {
        return Vote.ABSTAIN;
    }

    @Nonnull
    @Override
    public Vote canRead(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel) {
        return Vote.ABSTAIN;
    }

    @Nonnull
    @Override
    public Vote canAdmin(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel) {
        return Vote.ABSTAIN;
    }

    @Nonnull
    @Override
    public Vote canPublish(@Nonnull WebHookEvent webHookEvent, @Nonnull WebHookListener listener) {
        return Vote.ABSTAIN;
    }
}
