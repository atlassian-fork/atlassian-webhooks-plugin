package com.atlassian.webhooks.api.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate {@link com.atlassian.webhooks.spi.EventMatcher}
 * to indicate that you want all listeners to be considered for processing.
 * <p>
 * By default, only listeners explicitly registered for a particular
 * webhook are passed on to the matcher. If you don't want this
 * automatic pre-filtering then this annotation is for you.
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ConsiderAllListeners {
}