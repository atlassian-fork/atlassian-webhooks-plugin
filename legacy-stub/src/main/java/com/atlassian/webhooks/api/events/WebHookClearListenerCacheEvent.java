package com.atlassian.webhooks.api.events;

/**
 * Raise if you want webhook listener cache cleared.
 */
@Deprecated
public class WebHookClearListenerCacheEvent {
}
