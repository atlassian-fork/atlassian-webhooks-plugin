package com.atlassian.webhooks.api.events;

import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

@Deprecated
public final class WebHookPublishedEvent extends AbstractWebHookPublishResultEvent {
    public WebHookPublishedEvent(String webHookId, WebHookListenerRegistrationDetails registrationDetails) {
        super(webHookId, registrationDetails);
    }
}
