package com.atlassian.webhooks.spi;

import com.atlassian.webhooks.api.register.listener.WebHookListener;

/**
 * Additional condition for sending a webhook event to a listener.
 * <p>
 * In most basic case you just want to send a webhook event to
 * all listeners that registered for it. Use {@link com.atlassian.webhooks.api.util.EventMatchers#ALWAYS_TRUE} then.
 * <p>
 * However, you might have some events that allow additional filtering.
 * For example for issue related events we might let users provide some
 * custom JQL queries and accept only events that satisfy them.
 * <p>
 * It may happen, though rarely, that you want to consider all listeners stored
 * persistently, not only the ones registered for the event. If that is the case,
 * then mark your event matcher with {@link com.atlassian.webhooks.api.util.ConsiderAllListeners} annotation.
 */
@Deprecated
public interface EventMatcher<T> {
    /**
     * Tells whether the WebHook listener wants to accept a WebHook for the fired event.
     *
     * @param event    the event being fired, associated with the WebHook.
     * @param listener the listener waiting for the WebHook.
     * @return {@code true} if this event matches the web hook registration, {@code false} otherwise.
     */
    boolean matches(T event, WebHookListener listener);
}
