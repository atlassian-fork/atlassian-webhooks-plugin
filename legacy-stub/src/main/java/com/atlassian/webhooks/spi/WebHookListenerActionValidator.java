package com.atlassian.webhooks.spi;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.util.MessageCollection;

/**
 * This interface should be implemented by the provider which wants to validate the parameters of WebHook listener before
 * registration/update or removal.
 * There can be only one WebHookListenerActionValidator per product.
 */
@Deprecated
public interface WebHookListenerActionValidator {
    /**
     * Validates the parameters of newly registered WebHook listener. If this method returns any errors, WebHook listener
     * will not be registered.
     *
     * @param listener webhook listener
     * @return Collection with errors, if the validation failed. Empty collection if the validation passed.
     */
    MessageCollection validateWebHookRegistration(PersistentWebHookListener listener);

    /**
     * Validates the parameters of WebHook listener before the update. If this method returns any errors, WebHook listener
     * will not be updated.
     *
     * @param listener webhook listener
     * @return Collection with errors, if the validation failed. Empty collection if the validation passed.
     */
    MessageCollection validateWebHookUpdate(PersistentWebHookListener listener);

    /**
     * Validates the parameters of WebHook listener before the removal. If this method returns any errors, WebHook listener
     * will not be deleted.
     *
     * @param listener webhook listener
     * @return Collection with errors, if the validation failed. Empty collection if the validation passed.
     */
    MessageCollection validateWebHookRemoval(PersistentWebHookListener listener);
}
