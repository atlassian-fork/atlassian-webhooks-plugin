package com.atlassian.webhooks.request;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * HTTP request query parameter
 *
 * @since 5.0
 */
public interface QueryParameter {

    @Nonnull
    String getName();

    @Nonnull
    Optional<String> getValue();
}
