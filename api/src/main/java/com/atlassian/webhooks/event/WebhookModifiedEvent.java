package com.atlassian.webhooks.event;

import com.atlassian.event.api.AsynchronousPreferred;
import com.atlassian.webhooks.Webhook;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * This event is raised after an existing webhook has been modified.
 *
 * @since 5.0
 */
@AsynchronousPreferred
public class WebhookModifiedEvent extends AbstractWebhookEvent {

    private final Webhook newValue;
    private final Webhook oldValue;

    public WebhookModifiedEvent(@Nonnull Object source, @Nonnull Webhook oldValue, @Nonnull Webhook newValue) {
        super(requireNonNull(source, "source"));

        this.newValue = requireNonNull(newValue, "newWebhook");
        this.oldValue = requireNonNull(oldValue, "oldWebhook");
    }

    /**
     * Retrieves the <i>original</i> webhook values.
     *
     * @return the original webhook
     */
    @Nonnull
    public Webhook getOldValue() {
        return oldValue;
    }

    /**
     * Retrieves the <i>updated</i> webhook values.
     *
     * @return the updated webhook
     */
    @Nonnull
    public Webhook getNewValue() {
        return newValue;
    }
}
