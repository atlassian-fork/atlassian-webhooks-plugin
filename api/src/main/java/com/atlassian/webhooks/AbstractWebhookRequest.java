package com.atlassian.webhooks;

import com.atlassian.webhooks.util.BuilderUtil;
import org.hibernate.validator.constraints.URL;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * @since 5.0
 */
public abstract class AbstractWebhookRequest {

    private final boolean active;
    private final Map<String, String> configuration;
    private final List<WebhookEvent> events;
    private final String name;
    private final WebhookScope scope;
    private final String url;

    AbstractWebhookRequest(AbstractBuilder<?> builder) {
        this.active = builder.active;
        this.configuration = Collections.unmodifiableMap(new HashMap<>(builder.configuration));
        this.events = Collections.unmodifiableList(new ArrayList<>(builder.events));
        this.name = builder.name == null ? builder.url : builder.name;
        this.scope = builder.scope;
        this.url = builder.url;
    }

    @NotNull
    public Map<String, String> getConfiguration() {
        return configuration;
    }

    @Nonnull
    @NotEmpty(message = "{webhooks.event.required}")
    @Valid
    public List<WebhookEvent> getEvents() {
        return events;
    }

    @NotBlank(message = "{webhooks.field.name.required}")
    @Size(max = 255)
    public String getName() {
        return name;
    }

    @NotNull(message = "{webhooks.field.required}")
    public WebhookScope getScope() {
        return scope;
    }

    @NotNull(message = "{webhooks.field.url.required}")
    @URL
    public String getUrl() {
        return url;
    }

    public boolean isActive() {
        return active;
    }

    static abstract class AbstractBuilder<B extends AbstractBuilder<B>> {

        private final Map<String, String> configuration;
        private final List<WebhookEvent> events;

        private boolean active;
        private String name;
        private WebhookScope scope;
        private String url;

        AbstractBuilder() {
            this.configuration = new HashMap<>();
            this.active = true;
            this.events = new ArrayList<>();
        }

        @Nonnull
        public B active(boolean value) {
            active = value;

            return self();
        }

        /**
         * @see #configuration(String, String)
         */
        @Nonnull
        public B configuration(@Nullable Map<String, String> value) {
            if (value != null) {
                value.forEach(this::configuration);
            }
            return self();
        }

        /**
         * To be used for adding custom context or configuration to a webhook. Any configuration that has been added
         * for a create/update request will be persisted. It is also possible to set/update the context during a
         * webhook invocation. Context changed against an invocation will be ephemeral and any context that should stay
         * will need to be added via the {@link WebhookService#update} method.
         * <p>
         * When adding key/value pairs, to ensure that you do not collide with an existing key is is highly recommended
         * to prefix your keys with the package name.
         *
         * @see WebhookInvocation
         * @param key key of key/value pair to store
         * @param value value of key/value pair to store
         */
        @Nonnull
        public B configuration(@Nullable String key, @Nullable String value) {
            if (key != null) {
                if (value == null) {
                    configuration.remove(key);
                } else {
                    configuration.put(key, value);
                }
            }

            return self();
        }

        @Nonnull
        public B event(@Nullable Iterable<WebhookEvent> values) {
            BuilderUtil.addIf(Objects::nonNull, events, values);

            return self();
        }

        @Nonnull
        public B event(@Nullable WebhookEvent value, @Nullable WebhookEvent... values) {
            BuilderUtil.addIf(Objects::nonNull, events, value, values);

            return self();
        }

        @Nonnull
        public B name(@Nullable String value) {
            name = value;

            return self();
        }

        @Nonnull
        public B scope(@Nullable WebhookScope value) {
            scope = value;

            return self();
        }

        @Nonnull
        public B url(@Nullable String value) {
            url = value;

            return self();
        }

        @Nonnull
        protected abstract B self();
    }
}
