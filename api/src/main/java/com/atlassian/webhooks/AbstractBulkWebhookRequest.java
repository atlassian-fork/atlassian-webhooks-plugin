package com.atlassian.webhooks;

import com.atlassian.webhooks.util.BuilderUtil;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Request for bulk actions that take place against the {@link WebhookService},
 * such as deleting or searching.
 *
 * @since 5.0
 */
public abstract class AbstractBulkWebhookRequest {

    private final Boolean active;
    private final List<WebhookEvent> events;
    private final Integer id;
    private final String name;
    private final List<String> scopeTypes;
    private final List<WebhookScope> scopes;

    AbstractBulkWebhookRequest(AbstractBuilder<?> builder) {
        this.active = builder.active;
        this.events = Collections.unmodifiableList(new ArrayList<>(builder.events));
        this.id = builder.id;
        this.name = builder.name;
        this.scopes = Collections.unmodifiableList(new ArrayList<>(builder.scopes));
        this.scopeTypes = Collections.unmodifiableList(new ArrayList<>(builder.scopeTypes));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AbstractBulkWebhookRequest that = (AbstractBulkWebhookRequest) o;
        return Objects.equals(getActive(), that.getActive()) &&
                Objects.equals(getEvents(), that.getEvents()) &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(scopes, that.scopes) &&
                Objects.equals(scopeTypes, that.scopeTypes);
    }

    @Nullable
    public Boolean getActive() {
        return active;
    }

    @Nonnull
    public List<WebhookEvent> getEvents() {
        return events;
    }

    @Nullable
    public Integer getId() {
        return id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nonnull
    public List<String> getScopeTypes() {
        return scopeTypes;
    }

    @Nonnull
    public List<WebhookScope> getScopes() {
        return scopes;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getActive(), getId(), getName(), scopes, scopeTypes, getEvents());
    }

    static abstract class AbstractBuilder<B extends AbstractBuilder<B>> {

        private final List<WebhookEvent> events;
        private final List<String> scopeTypes;
        private final List<WebhookScope> scopes;
        private Boolean active;
        private Integer id;
        private String name;

        AbstractBuilder() {
            events = new ArrayList<>();
            scopes = new ArrayList<>();
            scopeTypes = new ArrayList<>();
        }

        AbstractBuilder(AbstractBulkWebhookRequest request) {
            this();

            active = requireNonNull(request, "request").getActive();
            events.addAll(request.getEvents());
            id = request.getId();
            name = request.getName();
            scopes.addAll(request.getScopes());
            scopeTypes.addAll(request.getScopeTypes());
        }

        @Nonnull
        public B active(boolean value) {
            this.active = value;

            return self();
        }

        @Nonnull
        public B event(@Nullable Iterable<WebhookEvent> values) {
            BuilderUtil.addIf(Objects::nonNull, events, values);

            return self();
        }

        @Nonnull
        public B event(@Nonnull WebhookEvent value, @Nonnull WebhookEvent... values) {
            BuilderUtil.addIf(Objects::nonNull, events, value, values);

            return self();
        }

        @Nonnull
        public B id(int value) {
            this.id = value;

            return self();
        }

        @Nonnull
        public B name(@Nonnull String value) {
            this.name = requireNonNull(value, "name");

            return self();
        }

        @Nonnull
        public B scope(@Nullable Iterable<WebhookScope> values) {
            BuilderUtil.addIf(Objects::nonNull, scopes, values);

            return self();
        }

        @Nonnull
        public B scope(@Nonnull WebhookScope value, @Nonnull WebhookScope... values) {
            BuilderUtil.addIf(Objects::nonNull, scopes, value, values);

            return self();
        }

        @Nonnull
        public B scopeType(@Nullable Iterable<String> values) {
            BuilderUtil.addIf(Objects::nonNull, scopeTypes, values);

            return self();
        }

        @Nonnull
        public B scopeType(@Nonnull String value, @Nonnull String... values) {
            BuilderUtil.addIf(Objects::nonNull, scopeTypes, value, values);

            return self();
        }

        @Nonnull
        protected abstract B self();
    }
}
