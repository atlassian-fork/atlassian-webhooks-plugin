package com.atlassian.webhooks;

/**
 * Exception that is thrown when a webhook is dispatched before the webhooks plugin has been fully initialized
 */
public class WebhooksNotInitializedException extends RuntimeException {

    public WebhooksNotInitializedException() {
        super();
    }

    public WebhooksNotInitializedException(String message) {
        super(message);
    }

    public WebhooksNotInitializedException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebhooksNotInitializedException(Throwable cause) {
        super(cause);
    }
}
