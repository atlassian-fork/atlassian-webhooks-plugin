package com.atlassian.webhooks;

/**
 * Exception that is throws when a webhook is expected to exist, but is not found
 *
 * @since 5.0
 */
public class NoSuchWebhookException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public NoSuchWebhookException() {
    }

    public NoSuchWebhookException(String message) {
        super(message);
    }
}
