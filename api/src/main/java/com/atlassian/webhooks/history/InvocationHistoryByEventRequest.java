package com.atlassian.webhooks.history;

import javax.annotation.Nonnull;

/**
 * A request object used to retrieved all per-event {@link InvocationHistory invocation histories}
 * for a single webhook in one go
 *
 * @since 6.1
 */
public class InvocationHistoryByEventRequest {

    private final int webhookId;

    private InvocationHistoryByEventRequest(Builder builder) {
        webhookId = builder.webhookId;
    }

    @Nonnull
    public static Builder builder(int webhookId) {
        return new Builder(webhookId);
    }

    public int getWebhookId() {
        return webhookId;
    }

    public static class Builder {

        private final int webhookId;

        public Builder(int webhookId) {
            this.webhookId = webhookId;
        }

        @Nonnull
        public InvocationHistoryByEventRequest build() {
            return new InvocationHistoryByEventRequest(this);
        }
    }
}
