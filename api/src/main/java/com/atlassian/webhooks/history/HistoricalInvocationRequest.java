package com.atlassian.webhooks.history;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.util.Optional.ofNullable;

/**
 * @since 6.1
 */
public class HistoricalInvocationRequest {

    private final String eventId;
    private final Set<InvocationOutcome> outcomes;
    private final int webhookId;

    private HistoricalInvocationRequest(Builder builder) {
        eventId = builder.eventId;
        outcomes = Collections.unmodifiableSet(new HashSet<>(builder.outcomes));
        webhookId = builder.webhookId;
    }

    @Nonnull
    public static Builder builder(int webhookId) {
        return new Builder(webhookId);
    }

    @Nonnull
    public Optional<String> getEventId() {
        return ofNullable(eventId);
    }

    @Nonnull
    public Collection<InvocationOutcome> getOutcomes() {
        return outcomes;
    }

    public int getWebhookId() {
        return webhookId;
    }

    public static class Builder {

        private final Set<InvocationOutcome> outcomes;
        private final int webhookId;

        private String eventId;

        public Builder(int webhookId) {
            this.webhookId = webhookId;

            outcomes = new HashSet<>();
        }

        @Nonnull
        public HistoricalInvocationRequest build() {
            return new HistoricalInvocationRequest(this);
        }

        @Nonnull
        public Builder eventId(@Nullable String value) {
            eventId = value;
            return this;
        }

        @Nonnull
        public Builder outcome(@Nullable Collection<InvocationOutcome> values) {
            if (values != null) {
                values.stream().filter(Objects::nonNull).forEach(outcomes::add);
            }
            return this;
        }

        @Nonnull
        public Builder outcome(@Nullable InvocationOutcome value, @Nonnull InvocationOutcome... moreValues) {
            if (value != null) {
                outcomes.add(value);
            }
            if (moreValues != null) {
                Arrays.stream(moreValues).filter(Objects::nonNull).forEach(outcomes::add);
            }
            return this;
        }
    }
}
