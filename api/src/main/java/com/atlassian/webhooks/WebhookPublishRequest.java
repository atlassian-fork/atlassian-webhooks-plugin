package com.atlassian.webhooks;

import com.atlassian.webhooks.util.BuilderUtil;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

/**
 * Used to request webhooks that match the search parameters are fired with the trigger event as the payload.
 *
 * @since 5.0
 */
public class WebhookPublishRequest {

    private final List<WebhookCallback> callbacks;
    private final WebhookEvent event;
    private final Object payload;

    private List<WebhookScope> scopes;
    private Webhook webhook;

    private WebhookPublishRequest(SearchBuilder searchBuilder) {
        this.callbacks = searchBuilder.callbacks;
        this.event = searchBuilder.event;
        this.scopes = searchBuilder.scopes;
        this.payload = searchBuilder.payload;
    }

    private WebhookPublishRequest(SingleWebhookBuilder singleWebhookBuilder) {
        this.callbacks = singleWebhookBuilder.callbacks;
        this.event = singleWebhookBuilder.event;
        this.payload = singleWebhookBuilder.payload;
        this.webhook = singleWebhookBuilder.webhook;
    }

    @Nonnull
    public static SearchBuilder builder(@Nonnull WebhookEvent event, @Nullable Object payload) {
        return new SearchBuilder(event, payload);
    }

    @Nonnull
    public static SingleWebhookBuilder builder(@Nonnull Webhook webhook, @Nonnull WebhookEvent event,
                                               @Nullable Object payload) {
        return new SingleWebhookBuilder(webhook, event, payload);
    }

    @Nonnull
    public List<WebhookCallback> getCallbacks() {
        return callbacks;
    }

    @NotNull
    public WebhookEvent getEvent() {
        return event;
    }

    @Nonnull
    public Optional<Object> getPayload() {
        return ofNullable(payload);
    }

    @Nonnull
    public List<WebhookScope> getScopes() {
        return scopes;
    }

    @Nonnull
    public Optional<Webhook> getWebhook() {
        return ofNullable(webhook);
    }

    public static class SearchBuilder {

        private final List<WebhookCallback> callbacks;
        private final Object payload;
        private final List<WebhookScope> scopes;

        private WebhookEvent event;

        public SearchBuilder(@Nonnull WebhookEvent event, @Nullable Object payload) {
            this.callbacks = new ArrayList<>();
            this.event = requireNonNull(event, "event");
            this.payload = payload;
            this.scopes = new ArrayList<>();
        }

        @Nonnull
        public WebhookPublishRequest build() {
            return new WebhookPublishRequest(this);
        }

        @Nonnull
        public SearchBuilder callback(@Nonnull WebhookCallback value, @Nonnull WebhookCallback... values) {
            BuilderUtil.addIf(Objects::nonNull, callbacks, value, values);

            return this;
        }

        @Nonnull
        public SearchBuilder scopes(@Nonnull WebhookScope value, @Nonnull WebhookScope... values) {
            BuilderUtil.addIf(Objects::nonNull, scopes, value, values);

            return this;
        }
    }

    public static class SingleWebhookBuilder {

        private final List<WebhookCallback> callbacks;
        private final WebhookEvent event;
        private final Object payload;
        private final Webhook webhook;

        public SingleWebhookBuilder(@Nonnull Webhook webhook, @Nonnull WebhookEvent event, @Nullable Object payload) {
            this.event = requireNonNull(event, "event");
            this.payload = payload;
            this.webhook = requireNonNull(webhook, "webhook");

            callbacks = new ArrayList<>();
        }

        @Nonnull
        public WebhookPublishRequest build() {
            return new WebhookPublishRequest(this);
        }

        @Nonnull
        public SingleWebhookBuilder callback(@Nonnull WebhookCallback value, @Nonnull WebhookCallback... values) {
            BuilderUtil.addIf(Objects::nonNull, callbacks, value, values);

            return this;
        }
    }
}
