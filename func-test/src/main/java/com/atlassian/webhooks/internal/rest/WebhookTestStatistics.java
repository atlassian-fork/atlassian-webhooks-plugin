package com.atlassian.webhooks.internal.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.concurrent.atomic.AtomicInteger;

@JsonSerialize
public class WebhookTestStatistics {

    private final AtomicInteger errorCount = new AtomicInteger(0);
    private final AtomicInteger failureCount = new AtomicInteger(0);
    private final AtomicInteger notDispatchedCount = new AtomicInteger(0);
    private final AtomicInteger successCount = new AtomicInteger(0);

    @JsonProperty
    public int getErrorCount() {
        return errorCount.get();
    }

    @JsonProperty
    public int getFailureCount() {
        return failureCount.get();
    }

    @JsonProperty
    public int getNotDispatchedCount() {
        return notDispatchedCount.get();
    }

    @JsonProperty
    public int getSuccessCount() {
        return successCount.get();
    }

    @Override
    public String toString() {
        return "WebhookTestStatistics{" +
                "errorCount=" + errorCount +
                ", failureCount=" + failureCount +
                ", notDispatchedCount=" + notDispatchedCount +
                ", successCount=" + successCount +
                '}';
    }

    void onDispatchFailure() {
        notDispatchedCount.incrementAndGet();
    }

    void onError() {
        errorCount.incrementAndGet();
    }

    void onFailure() {
        failureCount.incrementAndGet();
    }

    void onSuccess() {
        successCount.incrementAndGet();
    }
}
