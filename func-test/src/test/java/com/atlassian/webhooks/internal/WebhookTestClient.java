package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.internal.rest.WebhookTestStatistics;
import com.atlassian.webhooks.internal.rest.RestWebhook;
import com.atlassian.webhooks.internal.rest.RestWebhookScope;
import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.stream.Collectors;

public class WebhookTestClient {

    private static final String BASE_URL = System.getProperty("baseurl", "http://localhost:5990/refapp");
    private static final String REST_URL = BASE_URL + "/rest/webhooks-test/latest/";
    private static final String CALLBACKS_URL = REST_URL + "callbacks";
    private static final String TRIGGERS_URL = REST_URL + "triggers";
    private static final String WEBHOOKS_URL = REST_URL + "webhooks";

    private final ObjectMapper mapper;

    public WebhookTestClient() {
        mapper = new ObjectMapper();
    }

    public RestWebhook create(WebhookCreateRequest request) {
        Response response = RestAssured.given()
                .contentType("application/json")
                .body(toJson(request))
                .post(WEBHOOKS_URL);

        if (isSuccess(response)) {
            return response.getBody().as(RestWebhook.class);
        }
        return null;
    }

    public boolean delete(int webhookId) {
        Response response = RestAssured.given()
                .contentType("application/json")
                .delete(webhookUrl(webhookId));

        return isSuccess(response);
    }

    public int getCallbackCount() {
        Response response = RestAssured.given().contentType("application/json")
                .get(CALLBACKS_URL);

        return isSuccess(response) ? Integer.parseInt(response.body().asString()) : -1;
    }

    public WebhookTestStatistics getStatistics(String requestId) {
        Response response = RestAssured.given().contentType("application/json")
                .get(triggerUrl(requestId, "statistics"));

        return isSuccess(response) ? response.getBody().as(WebhookTestStatistics.class) : null;
    }

    public void resetCallbackCounter() {
        RestAssured.given().contentType("application/json")
                .delete(CALLBACKS_URL);
    }

    public boolean triggerMoonBaseCreated(String key) {
        Response response = RestAssured.given().contentType("application/json")
                .post(triggerUrl("moon-bases", key, "create"));

        return isSuccess(response);
    }

    public String triggerProjectCreate(String projectKey) {
        return triggerProjectCreate(projectKey, 1);
    }

    public String triggerProjectCreate(String projectKey, int count) {
        Response response = RestAssured.given().contentType("application/json")
                .post(triggerUrl("projects", projectKey, "create") + "?count=" + count);

        if (isSuccess(response)) {
            return response.getBody().asString();
        }
        return null;
    }

    private boolean isSuccess(Response response) {
        return response.getStatusCode() >= 200 && response.getStatusCode() < 300;
    }

    private String toJson(WebhookCreateRequest request) {
        try {
            return mapper.writeValueAsString(ImmutableMap.builder()
                    .put("name", request.getName())
                    .put("url", request.getUrl())
                    .put("scope", new RestWebhookScope(request.getScope()))
                    .put("events", request.getEvents().stream().map(WebhookEvent::getId).collect(Collectors.toList()))
                    .put("active", request.isActive())
                    .put("configuration", request.getConfiguration())
                    .build());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String triggerUrl(Object... pathComponents) {
        return TRIGGERS_URL + "/" + StringUtils.join(pathComponents, "/");
    }

    private String webhookUrl(int webhookId) {
        return WEBHOOKS_URL + "/" + webhookId;
    }
}
