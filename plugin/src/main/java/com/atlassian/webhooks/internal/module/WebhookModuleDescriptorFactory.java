package com.atlassian.webhooks.internal.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.webhooks.module.WebhookModuleDescriptor;
import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@ModuleType(ListableModuleDescriptorFactory.class)
public class WebhookModuleDescriptorFactory extends SingleModuleDescriptorFactory<SimpleWebhookModuleDescriptor> {

    @Autowired
    public WebhookModuleDescriptorFactory(HostContainer hostContainer) {
        super(hostContainer, "webhook", SimpleWebhookModuleDescriptor.class);
    }

    @Override
    public Set<Class<? extends ModuleDescriptor>> getModuleDescriptorClasses() {
        return ImmutableSet.of(SimpleWebhookModuleDescriptor.class, WebhookModuleDescriptor.class);
    }
}
