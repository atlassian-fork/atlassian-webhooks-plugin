package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.WebhookEventIdValidator;
import com.atlassian.webhooks.WebhookService;

/**
 * Registers the WebhookService with the WebhookEventIdValidator through the protected set-once setWebhookService.
 * This method is a bit hacky, but is used to ensure that {@link WebhookEventIdValidator} can provide a no-arg
 * public constructor.
 */
public class WebhookServiceRegistrar extends WebhookEventIdValidator {

    public void register(WebhookService value) {
        setWebhookService(value);
    }
}
