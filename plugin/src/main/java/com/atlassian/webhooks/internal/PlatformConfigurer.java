package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.WebhookRequestEnricher;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.internal.history.WebhookInvocationHistorian;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Component
public class PlatformConfigurer implements WebhooksLifecycleAware {

    private final BundleContext bundleContext;
    private final WebhookInvocationHistorian historian;

    private final List<ServiceRegistration<?>> serviceRegistrations;

    @Autowired
    public PlatformConfigurer(WebhookInvocationHistorian historian, BundleContext bundleContext) {
        this.bundleContext = bundleContext;
        this.historian = historian;

        serviceRegistrations = new ArrayList<>();
    }

    @Override
    public void onStart(WebhooksConfiguration configuration) {
        if (configuration.isInvocationHistoryEnabled()) {
            // only enable the WebhookInvocationHistorian if it's enabled in the configuration
            ServiceRegistration<?> registration = bundleContext.registerService(
                    WebhookRequestEnricher.class, historian, new Hashtable<>());
            serviceRegistrations.add(registration);
        }
    }

    @Override
    public void onStop() {
        serviceRegistrations.forEach(ServiceRegistration::unregister);
        serviceRegistrations.clear();
    }
}
