package com.atlassian.webhooks.internal.client.request;

import com.atlassian.webhooks.WebhookPayloadBuilder;
import com.atlassian.webhooks.request.Method;
import com.atlassian.webhooks.request.WebhookHttpRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.of;

public class RawRequest implements WebhookHttpRequest {

    private final byte[] content;
    private final String contentType;
    private final Map<String, String> headers;
    private final Method method;
    private final Map<String, List<String>> queryParameters;
    private final String url;

    private RawRequest(Builder builder) {
        content = builder.getBody();
        contentType = builder.getHeaders().getOrDefault("Content-Type", "application/json");
        headers = Collections.unmodifiableMap(new HashMap<>(builder.getHeaders()));
        method = builder.getMethod();
        queryParameters = Collections.unmodifiableMap(new HashMap<>(builder.getParameters()));
        url = builder.getUrl();
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public static Builder builder(@Nonnull Method post, @Nonnull String url) {
        return new Builder(post, url);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RawRequest that = (RawRequest) o;
        return Arrays.equals(getContent(), that.getContent()) &&
                Objects.equals(getHeaders(), that.getHeaders()) &&
                getMethod() == that.getMethod() &&
                Objects.equals(getQueryParameters(), that.getQueryParameters()) &&
                Objects.equals(getUrl(), that.getUrl());
    }

    @Nullable
    public byte[] getContent() {
        return content;
    }

    @Nonnull
    @Override
    public Optional<String> getContentType() {
        return of(contentType);
    }

    @Nonnull
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Nonnull
    public Method getMethod() {
        return method;
    }

    @Nonnull
    public Map<String, List<String>> getQueryParameters() {
        return queryParameters;
    }

    @Nonnull
    public String getUrl() {
        URI uri = URI.create(url);
        String queryParams = uri.getQuery();
        List<String> paramList = new ArrayList<>();
        getQueryParameters().forEach((key, value1) -> {
            for (String value : value1) {
                paramList.add(key + "=" + value);
            }
        });

        if (queryParams == null && !paramList.isEmpty()) {
            queryParams = String.join("&", paramList);
        } else if (paramList.size() > 0) {
            queryParams += "&" + String.join("&", paramList);

        }
        try {
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath(), queryParams, null).toASCIIString();
        } catch (URISyntaxException e) {
        }

        return "";
    }

    @Override
    public int hashCode() {
        return Objects.hash(getContent(), getHeaders(), getMethod(), getQueryParameters(), getUrl());
    }

    public static class Builder implements WebhookHttpRequest.Builder {

        private final Map<String, String> headers;
        private final Map<String, List<String>> parameters;

        private byte[] body;
        private Method method;
        private String url;

        private Builder() {
            headers = new HashMap<>();
            parameters = new HashMap<>();
        }

        private Builder(@Nonnull Method method, @Nonnull String url) {
            this();

            this.method = requireNonNull(method, "method");
            this.url = requireNonNull(url, "url");
        }

        @Nonnull
        public WebhookPayloadBuilder asPayloadBuilder() {
            return new PayloadBuilder();
        }

        @Nonnull
        @Override
        public RawRequest build() {
            return new RawRequest(this);
        }

        @Nullable
        @Override
        public byte[] getBody() {
            return body;
        }

        @Nonnull
        @Override
        public Map<String, String> getHeaders() {
            return Collections.unmodifiableMap(headers);
        }

        @Nonnull
        @Override
        public Method getMethod() {
            return method;
        }

        @Nonnull
        @Override
        public Map<String, List<String>> getParameters() {
            return Collections.unmodifiableMap(parameters);
        }

        @Nonnull
        @Override
        public String getUrl() {
            return url;
        }

        @Nonnull
        @Override
        public Builder header(@Nonnull String name, @Nullable String value) {
            headers.put(requireNonNull(name, "name"), value);
            return this;
        }

        @Nonnull
        @Override
        public Builder method(@Nonnull Method value) {
            method = requireNonNull(value, "method");
            return this;
        }

        @Nonnull
        @Override
        public Builder parameter(@Nonnull String name, @Nullable String... values) {
            parameters.putIfAbsent(name, new ArrayList<>());
            if (values != null) {
                Collections.addAll(parameters.get(name), values);
            }
            return this;
        }

        @Nonnull
        @Override
        public Builder removeHeader(@Nonnull String name) {
            headers.remove(requireNonNull(name, "name"));
            return this;
        }

        @Nonnull
        @Override
        public Builder url(@Nonnull String value) {
            url = requireNonNull(value, "url");
            return this;
        }

        @Nonnull
        @Override
        public Builder url(@Nonnull URI value) {
            url = requireNonNull(value, "url").toASCIIString();
            return this;
        }

        /**
         * {@link WebhookPayloadBuilder} that allows the body to be set once.
         */
        private class PayloadBuilder implements WebhookPayloadBuilder {

            @Nonnull
            @Override
            public PayloadBuilder body(@Nullable byte[] body, @Nullable String contentType) {
                Builder.this.body = body;
                if (contentType == null) {
                    headers.remove("Content-Type");
                } else {
                    headers.put("Content-Type", contentType);
                }
                return this;
            }

            @Nonnull
            @Override
            public PayloadBuilder header(@Nonnull String name, @Nullable String value) {
                Builder.this.header(name, value);
                return this;
            }
        }
    }
}
