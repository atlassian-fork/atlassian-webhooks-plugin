package com.atlassian.webhooks.internal.history;

import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.webhooks.NoSuchWebhookException;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.event.WebhookDeletedEvent;
import com.atlassian.webhooks.history.DetailedInvocationError;
import com.atlassian.webhooks.history.DetailedInvocationRequest;
import com.atlassian.webhooks.history.DetailedInvocationResponse;
import com.atlassian.webhooks.history.HistoricalInvocation;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationHistory;
import com.atlassian.webhooks.history.InvocationHistoryRequest;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.history.InvocationRequest;
import com.atlassian.webhooks.history.InvocationResult;
import com.atlassian.webhooks.internal.dao.AoInvocationHistoryDao;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;
import com.atlassian.webhooks.request.Method;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static com.atlassian.webhooks.internal.history.DefaultInvocationHistoryService.COUNTS_DAYS;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultInvocationHistoryServiceTest {

    @Mock
    private WebhooksConfiguration configuration;
    @Mock
    private AoInvocationHistoryDao dao;
    @Mock
    private EventListenerRegistrar eventListenerRegistrar;
    @InjectMocks
    private DefaultInvocationHistoryService service;
    @Mock
    private SchedulerService schedulerService;
    @Mock
    private TransactionTemplate transactionTemplate;
    @Mock
    private WebhookService webhookService;

    @Before
    public void setup() {
        when(dao.getCounts(anyInt(), anyString(), anyInt())).thenAnswer(invocation -> {
            int days = invocation.getArgument(2);
            return new SimpleInvocationCounts(Duration.of(days, ChronoUnit.DAYS), 0, 0, 0);
        });
        when(dao.getLatestInvocations(anyInt(), anyString(), any())).thenReturn(Collections.emptyList());
        when(webhookService.findById(anyInt())).thenReturn(empty());
        WebhookEvent event = mock(WebhookEvent.class);
        when(webhookService.getEvent("some:event")).thenReturn(of(event));
        when(transactionTemplate.execute(any())).then(invocation -> {
            TransactionCallback<?> callback = invocation.getArgument(0);
            return callback.doInTransaction();
        });
    }

    @Test
    public void testGetForHookAcrossEvents() {
        mockRegisteredWebhook(13);
        when(dao.getCounts(13, null, COUNTS_DAYS))
                .thenReturn(new SimpleInvocationCounts(Duration.of(COUNTS_DAYS, ChronoUnit.DAYS), 4, 0, 6));
        List<AoHistoricalInvocation> invocations = ImmutableList.of(
                mockInvocationWithResponse(InvocationOutcome.SUCCESS, 200),
                mockInvocationWithError());
        when(dao.getLatestInvocations(eq(13), eq(null), eq(null)))
                .thenReturn(invocations);

        InvocationHistory history = service.get(InvocationHistoryRequest.builder()
                .webhookId(13)
                .build());

        assertThat(history, notNullValue());
        assertErrorInvocation(history.getLastError().orElse(null));
        assertSuccessInvocation(history.getLastSuccess().orElse(null));
        assertThat(history.getLastFailure(), equalTo(empty()));
        assertCounts(history.getCounts(), 4, 0, 6);
    }

    @Test
    public void testGetForHookAndEvent() {
        mockRegisteredWebhook(13);
        when(dao.getCounts(13, "some:event", COUNTS_DAYS))
                .thenReturn(new SimpleInvocationCounts(Duration.of(COUNTS_DAYS, ChronoUnit.DAYS), 4, 0, 6));
        List<AoHistoricalInvocation> invocations = ImmutableList.of(
                mockInvocationWithResponse(InvocationOutcome.SUCCESS, 200),
                mockInvocationWithError());
        when(dao.getLatestInvocations(eq(13), eq("some:event"), eq(null)))
                .thenReturn(invocations);

        InvocationHistory history = service.get(InvocationHistoryRequest.builder()
                .eventId("some:event")
                .webhookId(13)
                .build());

        assertThat(history, notNullValue());
        assertErrorInvocation(history.getLastError().orElse(null));
        assertSuccessInvocation(history.getLastSuccess().orElse(null));
        assertThat(history.getLastFailure(), equalTo(empty()));
        assertCounts(history.getCounts(), 4, 0, 6);
    }

    @Test
    public void testGetForHookThatsNeverBeenCalled() {
        mockRegisteredWebhook(13);

        InvocationHistory history = service.get(InvocationHistoryRequest.builder()
                .webhookId(13)
                .build());

        assertThat(history, notNullValue());
        assertThat(history.getLastError(), equalTo(empty()));
        assertThat(history.getLastFailure(), equalTo(empty()));
        assertThat(history.getLastSuccess(), equalTo(empty()));
        assertCounts(history.getCounts(), 0, 0, 0);
    }

    @Test(expected = NoSuchWebhookException.class)
    public void testGetThrowsIfWebhookDoesNotExist() {
        service.get(InvocationHistoryRequest.builder()
                .webhookId(17)
                .build());
    }

    @Test
    public void testHistoryIsCleanedUpOnWebhookDeletion() {
        Webhook webhook = mock(Webhook.class, RETURNS_DEEP_STUBS);
        when(webhook.getId()).thenReturn(37);

        service.onWebhookDeleted(new WebhookDeletedEvent(this, webhook));

        verify(dao).deleteForWebhook(37);
    }

    @Test
    public void testLifecycle() throws SchedulerServiceException {
        verifyZeroInteractions(schedulerService);

        service.onStart(configuration);

        ArgumentCaptor<JobRunnerKey> keyCaptor = ArgumentCaptor.forClass(JobRunnerKey.class);
        verify(schedulerService).registerJobRunner(keyCaptor.capture(), any());
        verify(schedulerService).scheduleJob(any(), any());
        verify(eventListenerRegistrar).register(service);
        verifyNoMoreInteractions(schedulerService, eventListenerRegistrar);

        service.onStop();
        verify(schedulerService).unregisterJobRunner(eq(keyCaptor.getValue()));
        verify(eventListenerRegistrar).unregister(service);
        verifyNoMoreInteractions(schedulerService, eventListenerRegistrar);
    }

    private static void assertCounts(InvocationCounts counts, int errors, int failures, int successes) {
        assertThat(counts, notNullValue());
        assertThat(counts.getErrors(), equalTo(errors));
        assertThat(counts.getFailures(), equalTo(failures));
        assertThat(counts.getSuccesses(), equalTo(successes));
    }

    private static void assertDetailedError(InvocationResult result) {
        assertError(result);

        assertThat(result, instanceOf(DetailedInvocationError.class));
        DetailedInvocationError error = (DetailedInvocationError) result;
        assertThat(error.getContent(), equalTo("error-content"));
    }

    private static void assertDetailedErrorInvocation(HistoricalInvocation invocation) {
        assertInvocation(invocation);
        assertDetailedRequest(invocation.getRequest());
        assertDetailedError(invocation.getResult());
    }

    private static void assertDetailedRequest(InvocationRequest request) {
        assertRequest(request);
        assertThat(request, instanceOf(DetailedInvocationRequest.class));
        DetailedInvocationRequest detailedRequest = (DetailedInvocationRequest) request;
        assertThat(detailedRequest.getBody(), equalTo(of("request-body")));
        assertThat(detailedRequest.getHeaders(), equalTo(ImmutableMap.of(
                "req.header.1", "value-1",
                "req.header.2", "value-2")));
    }

    private static void assertDetailedSuccessInvocation(HistoricalInvocation invocation) {
        assertInvocation(invocation);
        assertDetailedRequest(invocation.getRequest());
        assertDetailedResponse(invocation.getResult(), InvocationOutcome.SUCCESS, 200);
    }

    private static void assertDetailedResponse(InvocationResult result, InvocationOutcome outcome, int status) {
        assertResponse(result, outcome);
        assertThat(result, instanceOf(DetailedInvocationResponse.class));
        DetailedInvocationResponse detailedResponse = (DetailedInvocationResponse) result;
        assertThat(detailedResponse.getBody(), equalTo(of("response-body")));
        assertThat(detailedResponse.getHeaders(), equalTo(ImmutableMap.of(
                "resp.header.1", "value-1",
                "resp.header.2", "value-2")));
        assertThat(detailedResponse.getStatusCode(), equalTo(status));
    }

    private static void assertError(InvocationResult result) {
        assertThat(result, notNullValue());
        assertThat(result.getOutcome(), equalTo(InvocationOutcome.ERROR));
        assertThat(result.getDescription(), equalTo("result-description"));
    }

    private static void assertErrorInvocation(HistoricalInvocation invocation) {
        assertInvocation(invocation);
        assertRequest(invocation.getRequest());
        assertError(invocation.getResult());
    }

    private static void assertInvocation(HistoricalInvocation invocation) {
        assertThat(invocation, notNullValue());
        assertThat(invocation.getDuration(), equalTo(Duration.of(40, ChronoUnit.SECONDS)));
        assertThat(invocation.getEvent(), notNullValue());
        assertThat(invocation.getFinish(), equalTo(Instant.ofEpochMilli(50000L)));
        assertThat(invocation.getId(), equalTo("request-id"));
        assertThat(invocation.getStart(), equalTo(Instant.ofEpochMilli(10000L)));
    }

    private static void assertResponse(InvocationResult result, InvocationOutcome outcome) {
        assertThat(result, notNullValue());
        assertThat(result.getOutcome(), equalTo(outcome));
        assertThat(result.getDescription(), equalTo("result-description"));
    }

    private static void assertSuccessInvocation(HistoricalInvocation invocation) {
        assertInvocation(invocation);
        assertRequest(invocation.getRequest());
        assertResponse(invocation.getResult(), InvocationOutcome.SUCCESS);
    }

    private static void assertRequest(InvocationRequest request) {
        assertThat(request, notNullValue());
        assertThat(request.getMethod(), equalTo(Method.POST));
        assertThat(request.getUrl(), equalTo("request-url"));
    }

    private static AoHistoricalInvocation mockInvocationWithError() {
        AoHistoricalInvocation i = mockInvocationWithRequestDetails(InvocationOutcome.ERROR);
        when(i.getErrorContent()).thenReturn("error-content");
        return i;
    }

    private static AoHistoricalInvocation mockInvocationWithResponse(InvocationOutcome outcome, int status) {
        AoHistoricalInvocation i = mockInvocationWithRequestDetails(outcome);
        when(i.getResponseBody()).thenReturn("response-body");
        when(i.getResponseHeaders()).thenReturn("resp.header.1=value-1\nresp.header.2=value-2");
        when(i.getStatusCode()).thenReturn(status);
        return i;
    }

    private static AoHistoricalInvocation mockInvocationWithRequestDetails(InvocationOutcome outcome) {
        AoHistoricalInvocation i = mock(AoHistoricalInvocation.class);
        when(i.getEventId()).thenReturn("event-id");
        when(i.getFinish()).thenReturn(50000L);
        when(i.getId()).thenReturn("id");
        when(i.getOutcome()).thenReturn(outcome);
        when(i.getRequestBody()).thenReturn("request-body");
        when(i.getRequestHeaders()).thenReturn("req.header.1=value-1\nreq.header.2=value-2");
        when(i.getRequestId()).thenReturn("request-id");
        when(i.getRequestMethod()).thenReturn("POST");
        when(i.getRequestUrl()).thenReturn("request-url");
        when(i.getResultDescription()).thenReturn("result-description");
        when(i.getStart()).thenReturn(10000L);
        when(i.getWebhookId()).thenReturn(99);
        return i;
    }

    private Webhook mockRegisteredWebhook(int id) {
        Webhook webhook = mock(Webhook.class);
        when(webhookService.findById(id)).thenReturn(of(webhook));

        return webhook;
    }
}