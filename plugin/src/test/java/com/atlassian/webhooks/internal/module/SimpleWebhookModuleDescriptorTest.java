package com.atlassian.webhooks.internal.module;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEventIdValidator;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.internal.TestEvent;
import com.atlassian.webhooks.internal.Validator;
import com.atlassian.webhooks.internal.WebhookServiceRegistrar;
import com.google.common.collect.ImmutableMap;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.*;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SimpleWebhookModuleDescriptorTest {

    private static final String PLUGIN_KEY = "com.atlassian.test.atlassian-test";
    private static final String BASE_URL = "http://app.domain.com:7121/context";

    private static final Map<String, String> CONFIG_EXLCUDE_BODY = ImmutableMap.of(
            "pluginKey", PLUGIN_KEY,
            "excludeBody", "true");

    private static ApplicationProperties applicationProperties;
    private static Validator validator;
    private static WebhookService webhookService;

    @Mock
    private ModuleFactory moduleFactory;
    @Mock
    private Plugin plugin;

    @BeforeClass
    public static void setupOnce() {
        applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn(BASE_URL);
        // mock the WebhookService
        webhookService = mock(WebhookService.class);
        when(webhookService.getEvents()).thenReturn(Arrays.asList(TestEvent.values()));
        when(webhookService.getEvent(anyString()))
                .thenAnswer(invocation -> ofNullable(TestEvent.fromId(invocation.getArgument(0))));

        // register the WebhookService so it's used for validations
        new WebhookServiceRegistrar().register(webhookService);

        // set up the jsr-303 validator for the test so validations can be verified as well
        ConstraintValidatorFactory defaultConstraintValidatorFactory = Validation.buildDefaultValidatorFactory()
                        .getConstraintValidatorFactory();
        ConstraintValidatorFactory constraintValidatorFactory =
                new TestConstraintValidatorFactory(defaultConstraintValidatorFactory);

        ValidatorFactory factory = Validation.byDefaultProvider().configure()
                .constraintValidatorFactory(constraintValidatorFactory)
                .buildValidatorFactory();
        javax.validation.Validator jsr303Validator = factory.getValidator();
        validator = new Validator() {
            @Override
            public <T> T validate(T object) {
                Set<ConstraintViolation<T>> violations = jsr303Validator.validate(object);
                if (!violations.isEmpty()) {
                    throw new ConstraintViolationException(violations);
                }
                return object;
            }
        };
    }

    @AfterClass
    public static void tearDownOnce() {
        new WebhookServiceRegistrar().register(null);
    }

    @Before
    public void setup() {
        when(plugin.getKey()).thenReturn(PLUGIN_KEY);
    }

    @Test
    public void testMinimalWebhook() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\"/>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getId(), lessThan(0));
        assertThat(webhook.getName(), equalTo("my-hook"));
        assertThat(webhook.getUrl(), equalTo("http://call.me/back"));
        assertThat(webhook.getCreatedDate(), notNullValue());
        assertThat(webhook.getUpdatedDate(), notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(ImmutableMap.of("pluginKey", PLUGIN_KEY)));
        assertThat(webhook.isActive(), equalTo(true));
        assertThat(webhook.getScope(), equalTo(WebhookScope.GLOBAL));
        assertThat(webhook.getEvents(), containsInAnyOrder(TestEvent.PROJ_CREATE));
    }

    @Test(expected = PluginParseException.class)
    public void testEventRequired() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\"/>");
        assertThat(webhook.isActive(), equalTo(false));
    }

    @Test
    public void testInvalidEvent() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"no_such:event\"/>");
        assertThat(webhook.isActive(), equalTo(false));
    }

    @Test
    public void testInvalidUrl() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"not a url\" event=\"project:create\"/>");
        assertThat(webhook.isActive(), equalTo(false));
    }

    @Test
    public void testParamSupport() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">" +
                "\t<param name=\"param1\">value1</param>\n" +
                "\t<param name=\"param2\">value2</param>\n" +
                "\t<param name=\"param3\" value=\"value3\"/>\n" +
                "</webhook>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(ImmutableMap.builder()
                .put("pluginKey", PLUGIN_KEY)
                .put("param1", "value1")
                .put("param2", "value2")
                .put("param3", "value3")
                .build()));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testRelativeUrlIsExpanded() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"relative/path\" event=\"project:create\"/>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getUrl(), equalTo(BASE_URL + "/relative/path"));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testScopeSupport() {
        Webhook webhook = getWebhook(
                "<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">" +
                        "\t<scope type=\"project\">BSERV</scope>\n" +
                        "</webhook>");

        assertThat(webhook, notNullValue());
        WebhookScope scope = webhook.getScope();
        assertThat(scope, notNullValue());
        assertThat(scope.getType(), equalTo("project"));
        assertThat(scope.getId(), equalTo(of("BSERV")));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test(expected = PluginParseException.class)
    public void testUrlRequired() {
        getWebhook("<webhook key=\"my-hook\" event=\"project:*\"/>");
    }

    @Test
    public void testWildcardEventSupport() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:*\"/>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getEvents(),
                containsInAnyOrder(TestEvent.PROJ_CREATE, TestEvent.PROJ_DELETE, TestEvent.PROJ_UPDATE));
        assertThat(webhook.isActive(), equalTo(true));
    }


    // ---------------------------------------------------------------------------------------------------------------
    // Test support for legacy attributes and parameters in the webhooks module descriptor
    // ---------------------------------------------------------------------------------------------------------------

    @Test
    public void testLegacyExcludeBodyParam() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">" +
                "\t<param name=\"excludeBody\">true</param>\n" +
                "</webhook>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(CONFIG_EXLCUDE_BODY));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testLegacyExcludeIssueDetailsParam() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">" +
                "\t<param name=\"excludeIssueDetails\">true</param>\n" +
                "</webhook>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(CONFIG_EXLCUDE_BODY));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testLegacyFilterParam() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">" +
                "\t<param name=\"filter\">someFilter</param>\n" +
                "</webhook>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(ImmutableMap.of(
                "pluginKey", PLUGIN_KEY,
                "filter", "someFilter")));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testLegacyJqlParam() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">" +
                "\t<param name=\"jql\">project=PKEY</param>\n" +
                "</webhook>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(ImmutableMap.of(
                "pluginKey", PLUGIN_KEY,
                "filter", "project=PKEY")));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testLegacyOptionalAttributes() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\" " +
                "filter=\"someFilter\" excludeBody=\"true\"/>");


        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(ImmutableMap.of(
                "pluginKey", PLUGIN_KEY,
                "filter", "someFilter",
                "excludeBody", "true")));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test
    public void testLegacyPropertyKeySupport() {
        Webhook webhook = getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">\n" +
                "\t<param name=\"propertyKey\">prop1</param>\n" +
                "\t<param name=\"propertyKey\">prop2</param>\n" +
                "</webhook>");

        assertThat(webhook, notNullValue());
        assertThat(webhook.getConfiguration(), equalTo(ImmutableMap.of(
                "pluginKey", PLUGIN_KEY,
                "propertyKey", "prop1,prop2")));
        assertThat(webhook.isActive(), equalTo(true));
    }

    @Test(expected = PluginParseException.class)
    public void testLegacyConflictingExcludeBody1() {
        getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">\n" +
                "\t<param name=\"excludeBody\">true</param>\n" +
                "\t<param name=\"excludeIssueDetails\">false</param>\n" +
                "</webhook>");
    }

    @Test(expected = PluginParseException.class)
    public void testLegacyConflictingExcludeBody2() {
        getWebhook(
                "<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\" excludeBody=\"false\">\n" +
                        "\t<param name=\"excludeBody\">true</param>\n" +
                        "</webhook>");
    }

    @Test(expected = PluginParseException.class)
    public void testLegacyConflictingExcludeBody3() {
        getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" " +
                " event=\"project:create\" excludeBody=\"false\">\n" +
                "\t<param name=\"excludeIssueDetails\">true</param>\n" +
                "</webhook>");
    }

    @Test(expected = PluginParseException.class)
    public void testLegacyConflictingFilters1() {
        getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" " +
                " event=\"project:create\" filter=\"filter1\">\n" +
                "\t<param name=\"filter\">filter2</param>\n" +
                "</webhook>");
    }

    @Test(expected = PluginParseException.class)
    public void testLegacyConflictingFilters2() {
        getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" " +
                " event=\"project:create\" filter=\"filter1\">\n" +
                "\t<param name=\"jql\">filter2</param>\n" +
                "</webhook>");
    }

    @Test(expected = PluginParseException.class)
    public void testLegacyConflictingFilters3() {
        getWebhook("<webhook key=\"my-hook\" url=\"http://call.me/back\" event=\"project:create\">\n" +
                "\t<param name=\"filter\">filter1</param>\n" +
                "\t<param name=\"jql\">filter2</param>\n" +
                "</webhook>");
    }

    private Webhook getWebhook(String xmlString) {
        Element element = toDomElement(xmlString);

        SimpleWebhookModuleDescriptor descriptor = new SimpleWebhookModuleDescriptor(
                applicationProperties, moduleFactory, validator, webhookService);
        descriptor.init(plugin, element);
        descriptor.enabled();

        return descriptor.getModule();
    }

    private static Element toDomElement(String xmlString) {
        SAXReader reader = new SAXReader();
        try {
            return reader.read(new StringReader(xmlString)).getRootElement();
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }

    private static class TestConstraintValidatorFactory implements ConstraintValidatorFactory {

        private final ConstraintValidatorFactory delegate;

        private TestConstraintValidatorFactory(ConstraintValidatorFactory delegate) {
            this.delegate = delegate;
        }

        @Override
        public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {
            if (WebhookEventIdValidator.class.equals(key)) {
                //noinspection unchecked
                return (T) new WebhookEventIdValidator();
            }
            return delegate.getInstance(key);
        }

        @Override
        public void releaseInstance(ConstraintValidator<?, ?> instance) {
            if (!(instance instanceof WebhookEventIdValidator)) {
                delegate.releaseInstance(instance);
            }

        }
    }
}