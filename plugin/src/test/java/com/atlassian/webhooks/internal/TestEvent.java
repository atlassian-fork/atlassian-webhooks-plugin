package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.WebhookEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public enum TestEvent implements WebhookEvent {

    PROJ_CREATE("project:create", "webhooks.project.create"),
    PROJ_DELETE("project:delete", "webhooks.project.delete"),
    PROJ_UPDATE("project:update", "webhooks.project.update"),
    USER_CREATE("user:create", "webhooks.user.create");

    private final String i18nKey;
    private final String id;

    TestEvent(String id, String i18nKey) {
        this.i18nKey = i18nKey;
        this.id = id;
    }

    @Nullable
    public static TestEvent fromId(String id) {
        for (TestEvent value : values()) {
            if (id.equalsIgnoreCase(value.getId())) {
                return value;
            }
        }
        return null;
    }

    @Nonnull
    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }
}
