package com.atlassian.webhooks.internal.client.request;

import com.atlassian.webhooks.request.Method;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RawRequestTest {

    @Test
    public void testAddingQueryParamsInUrlAndInBuilder() {
        RawRequest req = RawRequest.builder(Method.POST, "http://google.com?a=b")
                .parameter("b", "c", "d", "e")
                .build();

        String url = req.getUrl();
        assertThat(url, equalTo("http://google.com?a=b&b=c&b=d&b=e"));
    }

    @Test
    public void testUrlOfJustDomainHasNoQuery() {
        RawRequest req = RawRequest.builder(Method.POST, "http://google.com")
                .build();

        assertThat(req.getUrl(), equalTo("http://google.com"));
    }

    @Test
    public void testAddingUnicode() {
        RawRequest req = RawRequest.builder(Method.POST, "http://google.com?a=b")
                .parameter("👍", "🔥", "d", "e")
                .build();

        String url = req.getUrl();
        assertThat(url, equalTo("http://google.com?a=b&%F0%9F%91%8D=%F0%9F%94%A5&%F0%9F%91%8D=d&%F0%9F%91%8D=e"));
    }

    @Test
    public void testRawRequest() {
        RawRequest req = RawRequest.builder(Method.POST, "http://google.com?a=b").build();

        String url = req.getUrl();

        assertThat(url, equalTo("http://google.com?a=b"));
    }
}