package com.atlassian.webhooks.internal;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.webhooks.*;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsResult;
import com.atlassian.webhooks.event.WebhookCreatedEvent;
import com.atlassian.webhooks.event.WebhookDeletedEvent;
import com.atlassian.webhooks.event.WebhookModifiedEvent;
import com.atlassian.webhooks.internal.dao.WebhookDao;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.model.SimpleWebhook;
import com.atlassian.webhooks.internal.model.SimpleWebhookEvent;
import com.atlassian.webhooks.internal.model.UnknownWebhookEvent;
import com.atlassian.webhooks.internal.publish.InternalWebhookInvocation;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;
import com.atlassian.webhooks.module.WebhookModuleDescriptor;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.verification.VerificationWithTimeout;

import javax.annotation.Nonnull;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.atlassian.webhooks.internal.WebhookTestUtils.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultWebhookServiceTest {

    private static final WebhookEvent TEST_EVENT = new SimpleWebhookEvent("test:event");
    private static final VerificationWithTimeout TIMEOUT = timeout(2000);

    @Mock
    private WebhookDao dao;
    @Mock
    private WebhookDispatcher dispatcher;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private WebhookHostAccessor hostAccessor;
    @Captor
    private ArgumentCaptor<InternalWebhookInvocation> invocationCaptor;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private TransactionTemplate transactionTemplate;
    @Mock
    private Validator validator;
    @Mock
    private WebhookPayloadManager webhookPayloadManager;

    private DefaultWebhookService service;

    @Before
    public void setup() {
        when(hostAccessor.getEnrichers()).thenReturn(Collections.emptyList());
        when(hostAccessor.getFilters()).thenReturn(Collections.emptyList());
        when(hostAccessor.getEvent(anyString())).thenAnswer(invocation -> {
            String id = invocation.getArgument(0);
            WebhookEvent type = TestEvent.fromId(id);
            return type == null ? new UnknownWebhookEvent(id) : type;
        });
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebhookModuleDescriptor.class))
                .thenReturn(Collections.emptyList());

        when(validator.validate(any())).thenAnswer(invocation -> invocation.getArgument(0));

        AoWebhook standardAoWebhook = mockStandardAoWebhook();
        when(dao.create(any())).thenReturn(standardAoWebhook);
        when(dao.getById(anyInt())).thenReturn(standardAoWebhook);
        when(dao.search(any())).thenReturn(new AoWebhook[]{});

        when(transactionTemplate.execute(any())).then(invocation -> {
            TransactionCallback<?> callback = invocation.getArgument(0);
            return callback.doInTransaction();
        });

        service = new DefaultWebhookService(dao, dispatcher, eventPublisher, hostAccessor,
                pluginAccessor, transactionTemplate, validator, webhookPayloadManager);
        service.onStart(WebhooksConfiguration.DEFAULT);
    }

    @After
    public void tearDown() {
        service.onStop();
    }

    @Test
    public void testCreateFromRequest() {
        Webhook webhook = service.create(standardWebhookRequest());

        assertStandardWebhook(webhook);
        verify(validator).validate(any());
        verify(dao).create(any());

        ArgumentMatcher<WebhookCreatedEvent> matcher = (event) ->
                event.getWebhook().getUrl().equals(WebhookTestUtils.DEFAULT_URL);
        verify(eventPublisher).publish(argThat(matcher));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void testCreateNullRequestThrows() {
        service.create(null);
    }

    @Test
    public void testDelete() {
        AoWebhook webhook = mockStandardAoWebhook();
        when(dao.getById(99)).thenReturn(webhook);

        assertTrue(service.delete(99));
        verify(dao).delete(aryEq(new AoWebhook[]{ webhook }));

        ArgumentMatcher<WebhookDeletedEvent> matcher = (event) ->
                event.getWebhook().getUrl().equals(WebhookTestUtils.DEFAULT_URL);
        verify(eventPublisher).publish(argThat(matcher));
    }

    @Test
    public void testDeleteInBulk() {
        // page size = 250, mock total 350 webhooks
        AoWebhook[] page1 = new AoWebhook[250];
        AoWebhook[] page2 = new AoWebhook[100];
        for (int i = 0; i < 250; ++i) {
            page1[i] = mockStandardAoWebhook();
        }
        for (int i = 0; i < 100; ++i) {
            page2[i] = mockStandardAoWebhook();
        }

        when(dao.search(any())).thenReturn(page1, page2);

        service.delete(WebhookDeleteRequest.builder()
                .active(true)
                .scope(WebhookScope.GLOBAL)
                .event(TestEvent.PROJ_CREATE)
                .build());

        ArgumentCaptor<WebhookSearchRequest> searchCaptor = ArgumentCaptor.forClass(WebhookSearchRequest.class);
        verify(dao, times(2)).search(searchCaptor.capture());

        WebhookSearchRequest searchRequest = searchCaptor.getValue();
        assertEquals(Boolean.TRUE, searchRequest.getActive());
        assertEquals(250, searchRequest.getLimit());
        assertEquals(0, searchRequest.getOffset());
        assertEquals(Collections.singletonList(TestEvent.PROJ_CREATE), searchRequest.getEvents());
        assertEquals(Collections.singletonList(WebhookScope.GLOBAL), searchRequest.getScopes());
        assertNull(searchRequest.getName());
        assertEquals(Collections.emptyList(), searchRequest.getScopeTypes());

        verify(dao).delete(page1);
        verify(dao).delete(page2);
        verify(eventPublisher, times(350)).publish(any(WebhookDeletedEvent.class));
        verifyNoMoreInteractions(dao, eventPublisher);
    }

    @Test
    public void testSingleInvocationHasBodySet() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(100);
        when(webhook.getUrl()).thenReturn("http://example.com");
        SimpleWebhookEvent payload = new SimpleWebhookEvent("test", "event");
        service.publish(WebhookPublishRequest.builder(webhook, payload, payload)
                .build());

        verify(webhookPayloadManager, TIMEOUT).setPayload(argThat(argument -> argument.getWebhook().getId() == 100), any());
    }

    @Test
    public void testGetEvent() {
        WebhookEvent testEvent = new SimpleWebhookEvent("test:event");
        when(hostAccessor.getEvent(anyString())).thenAnswer(invocation -> {
            String id = invocation.getArgument(0);
            return id.equals(testEvent.getId()) ? testEvent : new UnknownWebhookEvent(id);
        });

        assertThat(service.getEvent("test:event"), equalTo(of(testEvent)));
        assertThat(service.getEvent("other:event"), equalTo(empty()));
    }

    @Test
    public void testPublishDispatchesWebhooks() {
        AoWebhook[] results = { mockStandardAoWebhook() };
        when(dao.search(eq(
                WebhookSearchRequest.builder()
                        .active(true)
                        .event(TEST_EVENT)
                        .scope(WebhookScope.GLOBAL)
                        .build())))
                .thenReturn(results);

        service.publish(WebhookPublishRequest.builder(TEST_EVENT, this).build());

        verify(validator).validate(any());
        verify(dispatcher, TIMEOUT).dispatch(any());
    }

    @Test
    public void testPublishDispatchesToWebhooksModules() {
        // mock a <webhook> module
        Webhook webhook = mock(Webhook.class);
        when(webhook.getEvents()).thenReturn(Collections.singleton(TEST_EVENT));
        when(webhook.getScope()).thenReturn(WebhookScope.GLOBAL);
        when(webhook.isActive()).thenReturn(true);
        when(webhook.getUrl()).thenReturn("http://send.me/callbacks");

        WebhookModuleDescriptor descriptor = mock(WebhookModuleDescriptor.class);
        when(descriptor.getModule()).thenReturn(webhook);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(WebhookModuleDescriptor.class)))
                .thenReturn(Collections.singletonList(descriptor));

        service.publish(WebhookPublishRequest.builder(TEST_EVENT, this).build());

        ArgumentCaptor<InternalWebhookInvocation> captor = ArgumentCaptor.forClass(InternalWebhookInvocation.class);
        verify(dispatcher, TIMEOUT).dispatch(captor.capture());

        InternalWebhookInvocation invocation = captor.getValue();
        assertThat(invocation.getWebhook(), equalTo(webhook));
    }

    @Test
    public void testPublishFilteringAllWebhooks() {
        WebhookFilter skipAll = new WebhookFilter() {
            @Override
            public boolean filter(@Nonnull WebhookInvocation webhook) {
                return false;
            }

            @Override
            public int getWeight() {
                return 0;
            }
        };
        when(hostAccessor.getFilters()).thenReturn(Collections.singletonList(skipAll));
        AoWebhook[] results = { mockStandardAoWebhook() };
        when(dao.search(any())).thenReturn(results);

        service.publish(WebhookPublishRequest.builder(TEST_EVENT, this).build());

        verifyNoMoreInteractions(dispatcher);
    }

    @Test
    public void testPingFiresSimpleWebhook() throws ExecutionException, InterruptedException, TimeoutException {
        Future<WebhookDiagnosticsResult> future = service.ping(PingRequest.builder("http://example.com").build());

        verify(dispatcher).dispatch(invocationCaptor.capture());
        InternalWebhookInvocation invocation = invocationCaptor.getValue();
        assertThat(invocation.getWebhook().getUrl(), equalTo("http://example.com"));
        assertThat(invocation.getCallbacks().size(), greaterThanOrEqualTo(1));

        assertFalse(future.isDone());

        // mock a successful invocation
        WebhookHttpRequest request = mock(WebhookHttpRequest.class);
        WebhookHttpResponse response = mock(WebhookHttpResponse.class);
        invocation.getCallbacks().forEach(callback -> callback.onSuccess(request, response, invocation));

        assertTrue(future.isDone());
        WebhookDiagnosticsResult result = future.get(1, TimeUnit.SECONDS);

        assertNotNull(result);
        assertFalse(result.isError());
        assertSame(request, result.getRequest());
        assertSame(response, result.getResponse());
        assertNull(result.getError());
    }

    @Test
    public void testPingEnriches() {
        WebhookRequestEnricher enricher = mock(WebhookRequestEnricher.class);
        when(hostAccessor.getEnrichers()).thenReturn(ImmutableList.of(enricher));

        String pingUrl = "http://example.com";
        service.ping(PingRequest.builder(pingUrl).build());

        verify(enricher).enrich(argThat(x -> x.getRequestBuilder().getUrl().equals(pingUrl)));
    }

    @Test
    public void testPublishFilteringSomeWebhooks() {
        WebhookFilter onlyTest = new WebhookFilter() {
            @Override
            public boolean filter(@Nonnull WebhookInvocation webhook) {
                return webhook.getWebhook().getId() == 1;
            }

            @Override
            public int getWeight() {
                return 0;
            }
        };
        when(hostAccessor.getFilters()).thenReturn(Collections.singletonList(onlyTest));

        Webhook webhook = SimpleWebhook.builder()
                .id(1)
                .url(WebhookTestUtils.DEFAULT_URL)
                .build();
        Webhook filteredWebhook = SimpleWebhook.builder()
                .id(2)
                .url(WebhookTestUtils.DEFAULT_URL)
                .build();

        service.publish(WebhookPublishRequest.builder(webhook, TEST_EVENT, this)
                .build());

        service.publish(WebhookPublishRequest.builder(filteredWebhook, TEST_EVENT, this)
                .build());

        ArgumentCaptor<InternalWebhookInvocation> invocation = ArgumentCaptor.forClass(InternalWebhookInvocation.class);
        verify(dispatcher, TIMEOUT).dispatch(invocation.capture());

        assertThat(invocation.getValue().getWebhook().getId(), is(1));
    }

    @Test
    public void testPublishSupportsEnriching() {
        WebhookRequestEnricher enricher1 = mock(WebhookRequestEnricher.class);
        WebhookRequestEnricher enricher2 = mock(WebhookRequestEnricher.class);
        when(hostAccessor.getEnrichers()).thenReturn(ImmutableList.of(enricher1, enricher2));
        AoWebhook[] results = { mockStandardAoWebhook() };
        when(dao.search(any())).thenReturn(results);
        // have enricher1 throw an exception to test that exceptions are handled
        doThrow(new IllegalStateException()).when(enricher1).enrich(any());

        service.publish(WebhookPublishRequest.builder(TEST_EVENT, this).build());

        InOrder inOrder = inOrder(enricher1, enricher2);
        inOrder.verify(enricher1, TIMEOUT).enrich(any());
        inOrder.verify(enricher2, TIMEOUT).enrich(any());
    }

    @Test(expected = WebhooksNotInitializedException.class)
    public void testPublishThrowsWhenNotInitialized() {
        service.onStop();

        // recreate the service without calling onStart
        service = new DefaultWebhookService(dao, dispatcher, eventPublisher, hostAccessor,
                pluginAccessor, transactionTemplate, validator, webhookPayloadManager);

        Webhook webhook = SimpleWebhook.builder().url("http://example.com").build();
        service.publish(WebhookPublishRequest.builder(webhook, TEST_EVENT, null).build());
    }

    @Test
    public void testPublishWithStatisticsDisabled() {
        Webhook webhook = SimpleWebhook.builder().url("http://example.com").build();
        service.setStatisticsEnabled(false);
        assertThat(service.getStatistics(), equalTo(empty()));

        service.publish(WebhookPublishRequest.builder(webhook, TEST_EVENT, null).build());

        verify(dispatcher, TIMEOUT).dispatch(invocationCaptor.capture());

        // verify that no statistics gather callback is registered
        InternalWebhookInvocation invocation = invocationCaptor.getValue();
        assertThat(invocation.getCallbacks(), not(hasItem(instanceOf(SimpleWebhooksStatistics.StatisticsCallback.class))));
        assertThat(service.getStatistics(), equalTo(empty()));
    }

    @Test
    public void testPublishWithStatisticsEnabled() {
        Webhook webhook = SimpleWebhook.builder().url("http://example.com").build();
        service.setStatisticsEnabled(true);
        assertStatistics(0, 0, 0, 0, 0, 0);

        service.publish(WebhookPublishRequest.builder(webhook, TEST_EVENT, null).build());

        verify(dispatcher, TIMEOUT).dispatch(invocationCaptor.capture());

        // verify that statistics gathering callback is registered
        InternalWebhookInvocation invocation = invocationCaptor.getValue();
        WebhookCallback callback = getStatisticsCallback(invocation);
        assertThat(callback, notNullValue());

        // 1 published, but nothing dispatched yet
        assertStatistics(0, 0, 0, 0, 0, 1);

        WebhookHttpRequest request = mock(WebhookHttpRequest.class);
        WebhookHttpResponse response = mock(WebhookHttpResponse.class);

        // mock a dispatch success
        callback.onSuccess(request, response, invocation);
        assertStatistics(1, 0, 0, 0, 1, 1);

        // and a dispatch failure
        callback.onFailure(request, response, invocation);
        assertStatistics(2, 0, 1, 0, 1, 1);

        // and a dispatch rejected
        callback.onError(request, new DispatchFailedException(invocation, ""), invocation);
        assertStatistics(2, 0, 1, 1, 1, 1); // no increase in dispatchCount

        // and a dispatch error
        callback.onError(request, new SocketTimeoutException(), invocation);
        assertStatistics(3, 1, 1, 1, 1, 1);
    }

    @Test
    public void testSearch() {
        AoWebhook[] results = { mockStandardAoWebhook() };
        when(dao.search(any())).thenReturn(results);

        WebhookSearchRequest request = WebhookSearchRequest.builder()
                .event(TestEvent.PROJ_CREATE, TestEvent.PROJ_DELETE)
                .build();
        List<Webhook> webhooks = service.search(request);

        verify(dao).search(request);
        assertEquals(1, webhooks.size());
        assertStandardWebhook(webhooks.get(0));
    }

    @Test
    public void testSearchNoResults() {
        when(dao.search(any())).thenReturn(new AoWebhook[]{});

        List<Webhook> webhooks = service.search(WebhookSearchRequest.builder().build());

        assertNotNull(webhooks);
        assertEquals(0, webhooks.size());
    }

    @Test
    public void testUpdate() {

        String newUrl = "http://call.me/back";
        WebhookUpdateRequest request = WebhookUpdateRequest.builder()
                .event(new SimpleWebhookEvent("event1"), new SimpleWebhookEvent("event2"))
                .scope(WebhookScope.GLOBAL)
                .url(newUrl)
                .build();

        AoWebhook webhook = mockStandardAoWebhook();
        when(webhook.getID()).thenReturn(99);
        when(webhook.getUrl()).thenReturn(request.getUrl());

        when(dao.update(99, request)).thenReturn(webhook);

        Webhook result = service.update(99, request);
        assertEquals(99, result.getId());

        verify(validator).validate(any());
        verify(dao).getById(99);
        verify(dao).update(99, request);
        verifyNoMoreInteractions(dao);

        ArgumentMatcher<WebhookModifiedEvent> matcher = (event) ->
                     event.getOldValue().getUrl().equals(WebhookTestUtils.DEFAULT_URL)
                            && event.getNewValue().getUrl().equals(newUrl);
        verify(eventPublisher).publish(argThat(matcher));
    }

    @Test(expected = NoSuchWebhookException.class)
    public void testUpdateNonExistingThrows() {
        service.update(1234, WebhookUpdateRequest.builder()
                .event(new SimpleWebhookEvent("event1"), new SimpleWebhookEvent("event2"))
                .scope(WebhookScope.GLOBAL)
                .url("http://call.me/back")
                .build());
    }

    private WebhookCallback getStatisticsCallback(InternalWebhookInvocation invocation) {
        return invocation.getCallbacks().stream()
                .filter(callback ->  callback instanceof SimpleWebhooksStatistics.StatisticsCallback)
                .findFirst()
                .orElse(null);
    }

    private void assertStatistics(long dispatched, long dispatchErrors, long dispatchFailures,
                                  long dispatchesRejected, long dispatchSuccesses, long published) {
        WebhookStatistics statistics = service.getStatistics().orElse(null);

        assertThat(statistics, notNullValue());
        assertThat(statistics.getDispatchedCount(), equalTo(dispatched));
        assertThat(statistics.getDispatchErrorCount(), equalTo(dispatchErrors));
        assertThat(statistics.getDispatchFailureCount(), equalTo(dispatchFailures));
        assertThat(statistics.getDispatchRejectedCount(), equalTo(dispatchesRejected));
        assertThat(statistics.getDispatchSuccessCount(), equalTo(dispatchSuccesses));
        assertThat(statistics.getPublishedCount(), equalTo(published));
    }
}
