package com.atlassian.webhooks.util;

import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WebhookScopeUtilTest {

    @Test
    public void testEquality() {
        SimpleWebhookScope scope = new SimpleWebhookScope("A", "B");
        assertTrue(WebhookScopeUtil.equals(scope, scope));
        assertTrue(WebhookScopeUtil.equals(scope, new SimpleWebhookScope("A", "B")));
        assertTrue(WebhookScopeUtil.equals(new SimpleWebhookScope("type", null), new SimpleWebhookScope("type", null)));
        assertTrue(WebhookScopeUtil.equals(null, null));
    }

    @Test
    public void testInequality() {
        SimpleWebhookScope scope = new SimpleWebhookScope("A", "B");
        assertFalse(WebhookScopeUtil.equals(null, scope));
        assertFalse(WebhookScopeUtil.equals(scope, null));
        assertFalse(WebhookScopeUtil.equals(new SimpleWebhookScope("A", null), scope));
        assertFalse(WebhookScopeUtil.equals(new SimpleWebhookScope("A", "C"), scope));
    }
}